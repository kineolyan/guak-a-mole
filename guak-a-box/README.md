Guak-a-box
======

Simple game box, with several games.

Development
---------

 - Install Platformio CLI
 - Compile: `pio run` (do `pio run -t clean` to clean compiled objects)
 - Upload: `pio run -t upload` (with the board connected)
