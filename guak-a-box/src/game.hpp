#ifndef _GAME_H_
#define _GAME_H_

class GameIntf
{
public:
    virtual ~GameIntf() {}
    virtual void init();
    virtual void loop(const unsigned long currentMillis);
};

template <class State>
class Game : public GameIntf
{
private:
    State s1;
    State s2;
    State *s;

protected:
    virtual void initState(State &state);
    virtual void copy(State const &source, State &target);
    virtual void doLoop(const unsigned long currentMillis, State const &previous, State &next);

public:
    ~Game() {}

    void init()
    {
        initState(s1);
        s = &s1;
    }

    void loop(const unsigned long currentMillis)
    {
        State &previousState = *s;
        s = s == &s1 ? &s2 : &s1;
        State &nextState = *s;

        copy(previousState, nextState);
        doLoop(currentMillis, previousState, nextState);
    }
};

#endif
