#include "./guak.h"

#include "Arduino.h"
#include "./board.h"

namespace guak
{
    const unsigned long gameDuration = 60UL * 1000UL;

    struct TurnState
    {
        int score;
        unsigned long currentTime;
        unsigned long endTime;
        int ledId;

        TurnState(int score, unsigned long currentTime, unsigned long endTime, int ledId)
            : score(score), currentTime(currentTime), endTime(endTime), ledId(ledId)
        {
        }

        /**
         * @brief Returns the remaining time for the game.
         * 
         * @return time in seconds
         */
        unsigned long remainingTime() const
        {
            return (this->endTime - this->currentTime) / 1000;
        }
    };
    
    struct GameRule {
        static unsigned long offsetAfterPressed(TurnState const &turn) 
        {
            auto const rem = turn.remainingTime();
            if (rem > 30)
            {
                return random(2500, 5000);
            }
            else if (rem > 15)
            {
                return random(1000, 4000);
            }
            else
            {
                return random(500, 3000);
            }
        }

        static unsigned long offsetForGuess(TurnState const &turn)
        {
            auto const rem = turn.remainingTime();
            if (rem > 30)
            {
                return random(1000, 5000);
            }
            else if (rem > 15)
            {
                return random(500, 4000);
            }
            else
            {
                return random(500, 2000);
            }
        }

        static unsigned long offsetOnMissed(TurnState const &turn)
        {
            auto const rem = turn.remainingTime();
            if (rem > 30)
            {
                return random(500, 5000);
            }
            else if (rem > 15)
            {
                return random(500, 3750);
            }
            else
            {
                return random(500, 2500);
            }
        }
    };

    void playStartAnimation()
    {
        board::lightAllOn();
        for (int i = LED_COUNT - 1; i >= 0; i -= 1)
        {
            delay(500);
            board::lightOff(i);
        }
        for (int i = 0; i < 2; i += 1)
        {
            delay(50);
            board::lightAllOn();
            delay(50);
            board::lightAllOff();
        }
    }

    void Guak::initState(GameState &state)
    {
        Serial.println("Starting boot animation");
        playStartAnimation();

        Serial.println("Initializing Guak");
        // Setup the first timers
        unsigned long currentTime = millis();
        state.endTime = currentTime + gameDuration;
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            LedState &led = state.leds[i];
            led.timer = currentTime + random(1000, 5000);
            led.status = OFF;
        }

        board::lightAllOff();
        Serial.println("Guak initialized");
    }

    void copyLed(LedState const &source, LedState &target)
    {
        target.status = source.status;
        target.timer = source.timer;
    }

    void Guak::copy(GameState const &source, GameState &target)
    {
        target.score = source.score;
        target.endTime = source.endTime;
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            copyLed(source.leds[i], target.leds[i]);
        }
    }

    int handleLedOn(TurnState const &turn, LedState const &previous, LedState &next)
    {
        if (board::isPressed(turn.ledId))
        {
            next.status = PRESSED;
            next.timer = turn.currentTime + 10;
            return 1;
        }
        else if (turn.currentTime >= previous.timer)
        {
            board::lightOff(turn.ledId);
            next.status = OFF;
            next.timer = turn.currentTime + GameRule::offsetOnMissed(turn);
            return 0;
        }
        else
        {
            return 0;
        }
    }

    int handleLedOff(TurnState const &turn, LedState const &previous, LedState &next)
    {
        if (board::isPressed(turn.ledId))
        {
            next.status = PRESSED;
            next.timer = turn.currentTime + 10;
            return -1;
        }
        else if (turn.currentTime >= previous.timer)
        {
            board::lightOn(turn.ledId);
            next.status = ON;
            next.timer = turn.currentTime + GameRule::offsetForGuess(turn);
            return 0;
        }
        else
        {
            return 0;
        }
    }

    int handleLedPressed(TurnState const &turn, LedState const &previous, LedState &next)
    {
        // Give some time to avoid twitches
        if (turn.currentTime >= previous.timer && !board::isPressed(turn.ledId))
        {
            board::lightOff(turn.ledId); // Not always needed, but simpler
            next.status = OFF;
            next.timer = turn.currentTime + GameRule::offsetAfterPressed(turn);
        }
        return 0;
    }

    int loopOnLed(TurnState const &turn, LedState const &previous, LedState &next)
    {
        switch (previous.status)
        {
        case ON:
            return handleLedOn(turn, previous, next);
        case PRESSED:
            return handleLedPressed(turn, previous, next);
        case OFF:
            return handleLedOff(turn, previous, next);
        default:
            return 0;
        }
    }

    void playEndAnimation()
    {
        // Nice blinking
        for (int i = 0; i < 3; i += 1)
        {
            board::lightAllOn();
            delay(1000);
            board::lightAllOff();
            delay(150);
        }
        // Light on in a circle and light off
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            board::lightOn(i);
            delay(200);
        }
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            board::lightOff(i);
            delay(200);
        }
        board::lightAllOn();
        delay(100);
        board::lightAllOff();
    }

    void Guak::doLoop(const unsigned long currentMillis, GameState const &previous, GameState &next)
    {
        if (currentMillis < previous.endTime)
        {
            TurnState state(previous.score, currentMillis, previous.endTime, 0);
            int score = previous.score;
            for (int i = 0; i < LED_COUNT; i += 1)
            {
                state.ledId = i;
                int const scoreUpdate = loopOnLed(state, previous.leds[i], next.leds[i]);
                if (scoreUpdate != 0)
                {
                    // Use next score to handle multiple updates from all LEDs
                    score = max(score + scoreUpdate, 0);
                    Serial.print("new score = ");
                    Serial.println(score);
                }
            }
            next.score = score;
        }
        else
        {
            if (currentMillis < previous.endTime + 1000)
            {
                Serial.println("Game ended");
                // Just ended, play the end sequence
                playEndAnimation();
            } // else nothing to do, just stall the CPU
        }
    }

}
