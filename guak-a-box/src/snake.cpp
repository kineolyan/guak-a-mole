#include "./snake.h"

#include "Arduino.h"
#include "./constants.h"
#include "./board.h"

namespace snake
{

    int pickNextLed(const int currentLed)
    {
        for (int i = 0; i < 100; i += 1)
        {
            const int led = random(0, LED_COUNT);
            if (led != currentLed)
            {
                return led;
            }
        }
        // Backoff to a non random choice to avoid an infinite loop
        return (currentLed + 1) % LED_COUNT;
    }

    void setupNextLit(GameState const& current, GameState &next)
    {
        next.previousLed = current.targetLed;
        next.targetLed = pickNextLed(current.targetLed);
        next.state = PRESSED;
    }

    void awaitPress(GameState const &current, GameState &next)
    {
        if (board::isPressed(current.targetLed))
        {
            setupNextLit(current, next);
            board::lightOn(next.targetLed);
        }
    }

    void awaitRelease(GameState const &current, GameState &next)
    {
        if (!board::isPressed(current.previousLed))
        {
            board::lightOff(current.previousLed);
            if (!board::hasLedPressed())
            {
                next.state = LIT;
            }
            else
            {
                next.state = WAIT_CLEAR;
            }
        }
    }

    void awaitClear(GameState &next)
    {
        if (!board::hasLedPressed())
        {
            next.state = LIT;
        }
    }

    void Snake::initState(GameState &state)
    {
        state.state = LIT;
        state.targetLed = pickNextLed(-1);
        board::lightOn(state.targetLed);
        Serial.print("Init to ");
        Serial.println(state.targetLed);
    }

    void Snake::copy(GameState const &source, GameState &target)
    {
        target.state = source.state;
        target.targetLed = source.targetLed;
        target.previousLed = source.previousLed;
    }

    void Snake::doLoop(const unsigned long currentMillis, GameState const &current, GameState &next)
    {
        switch (current.state)
        {
        case LIT:
            awaitPress(current, next);
            break;
        case PRESSED:
            awaitRelease(current, next);
            break;
        case WAIT_CLEAR:
            awaitClear(next);
            break;
        }
    }

} // namespace snake
