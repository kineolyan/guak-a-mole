#include "./flood.h"

#include "Arduino.h"
#include "./constants.h"
#include "./board.h"

namespace flood
{

    bool sealIfNeeded(State const &current, State &next, int const id)
    {
        if (current.led[id] && board::isPressed(id))
        {
            next.led[id] = false;
            board::lightOff(id);
            Serial.println("One light off");
            return true;
        }
        else
        {
            return false;
        }
    }

    bool allLedsLeaking(State const &state)
    {
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            if (!state.led[i])
            {
                return false;
            }
        }
        return true;
    }

    int pickNextLed(State const &state)
    {
        for (int i = 0; i < 100; i += 1)
        {
            const int id = random(0, LED_COUNT);
            if (!state.led[id])
            {
                return id;
            }
        }

        // Pick the first led not lit
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            if (!state.led[i])
            {
                return i;
            }
        }
        return 0; // Not reachable
    }

    void lightNextLed(State const &current, State &next)
    {
        const int id = pickNextLed(current);
        next.led[id] = true;
        board::lightOn(id);
        Serial.print("Light on for ");
        Serial.println(id);
    }

    unsigned long getNextTarget(unsigned long const currentMillis, State const &state)
    {
        return currentMillis + 1000 - 7 * state.iterations;
    }

    void Flood::initState(State &state)
    {
        state.lost = false;
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            state.led[i] = false;
        }
        state.iterations = 0;
        state.target = 0;
    }

    void Flood::copy(State const &source, State &target)
    {
        target.lost = source.lost;
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            target.led[i] = source.led[i];
        }
        target.iterations = source.iterations;
        target.target = source.target;
    }

    void Flood::doLoop(const unsigned long currentMillis, State const &current, State &next)
    {
        if (!current.lost)
        {
            bool changed = false;
            for (int i = 0; i < LED_COUNT; i += 1)
            {
                changed |= sealIfNeeded(current, next, i);
            }

            if (changed)
            {
                next.iterations += 1;
                Serial.print("some changed...");
                Serial.println(next.iterations);
                return; // Loop to swap next as current
            }

            if (current.target < currentMillis)
            {
                if (allLedsLeaking(current))
                {
                    Serial.println("lost :-(");
                    next.lost = true;
                }
                else
                {
                    next.target = getNextTarget(currentMillis, current);
                    lightNextLed(current, next);
                    Serial.print("next in ->");
                    Serial.println(next.target);
                }
            }
        }
    }

} // namespace flood
