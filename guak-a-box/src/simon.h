#ifndef _SIMON_H_
#define _SIMON_H_

#include "./game.hpp"
#include "./constants.h"

#define SEQ_SIZE 128

namespace simon
{

    enum Stage
    {
        INIT,
        TEACH_ON,
        TEACH_OFF,
        TEACH_COMPLETE,
        WAIT_PRESS,
        WAIT_RELEASE,
        WON_ROUND,
        DONE
    };

    struct GameState
    {
        int seq[SEQ_SIZE];
        Stage stage;
        int endIdx;
        int currentIdx;
        unsigned long nextTime;
    };

    class Simon : public Game<GameState>
    {
        void initState(GameState &state);
        void copy(GameState const &source, GameState &target);
        void doLoop(const unsigned long currentMillis, GameState const &previous, GameState &next);
    };

} // namespace simon

#endif
