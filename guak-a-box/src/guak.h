#ifndef _GUAK_H_
#define _GUAK_H_

#include "./game.hpp"
#include "./constants.h"

namespace guak
{
    enum LedStatus {
        ON,
        PRESSED,
        OFF,
    };
    
    struct LedState {
        LedStatus status;
        unsigned long timer;
    };

    struct GameState
    {
        unsigned long endTime;
        int score;
        LedState leds[LED_COUNT];
    };

    class Guak : public Game<GameState>
    {
        void initState(GameState &state);
        void copy(GameState const &source, GameState &target);
        void doLoop(const unsigned long currentMillis, GameState const &previous, GameState &next);
    };

} // namespace guak

#endif
