#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#define LED_COUNT 5

const int ledPins[] =  {
  11, // red
  12, // white
  9,  // blue
  10, // yellow
  13  // green
};
const int btnPins[] = {
  4, // red
  5, // white
  2, // blue
  3, // yellow
  6  // green
};

const int POT_PIN = 1; // A1
const int START_PIN = 52;
const int START_POWER = 53;

#endif
