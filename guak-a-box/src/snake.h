#ifndef _SNAKE_H_
#define _SNAKE_H_

#include "./game.hpp"

namespace snake
{

    enum State
    {
        LIT,
        PRESSED,
        WAIT_CLEAR
    };

    typedef struct GameState
    {
        State state;
        int previousLed;
        int targetLed;
    } GameState;

    class Snake : public Game<GameState>
    {
        void initState(GameState &state);
        void copy(GameState const &source, GameState &target);
        void doLoop(const unsigned long currentMillis, GameState const &previous, GameState &next);
    };

} // namespace snake

#endif
