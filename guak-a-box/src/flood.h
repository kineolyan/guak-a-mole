#ifndef _FLOOD_H_
#define _FLOOD_H_

#include "./game.hpp"
#include "./constants.h"

namespace flood
{

    typedef struct State
    {
        bool lost;
        bool led[LED_COUNT];
        short iterations;
        unsigned long target;
    } State;

    class Flood : public Game<State>
    {
        void initState(State &state);
        void copy(State const &source, State &target);
        void doLoop(const unsigned long currentMillis, State const &previous, State &next);
    };

} // namespace flood

#endif
