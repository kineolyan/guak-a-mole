#include "Arduino.h"

#include "./constants.h"
#include "./board.h"
#include "./game.hpp"
#include "./snake.h"
#include "./flood.h"
#include "./simon.h"
#include "./guak.h"

typedef struct BoardState
{
  bool started;
  int gameKnob;
} BoardState;

void blinkAtStart(const int n)
{
  for (int i = 0; i < n; i += 1)
  {
    if (i > 0)
    {
      delay(50);
    }
    board::lightAllOn();
    delay(100);
    board::lightAllOff();
  }
}

void powerUp(const int pin)
{
  pinMode(pin, OUTPUT);
  digitalWrite(pin, HIGH);
}

void switchStart(BoardState &state)
{
  const int sensorVal = digitalRead(START_PIN);
  const bool pressed = sensorVal == 1;
  if (pressed != state.started)
  {
    state.started = pressed;
    if (Serial)
    {
      if (state.started)
      {
        Serial.println("Game started");
      }
      else
      {
        Serial.println("Game off");
      }
    }
  }
}

void readSelectedGame(BoardState &state)
{
  const int value = analogRead(POT_PIN);
  if (abs(value - state.gameKnob) >= 5)
  {
    state.gameKnob = value;
  }
}

void initBoard(BoardState &state)
{
  switchStart(state);
  readSelectedGame(state);
}

void copyBoard(BoardState const &source, BoardState &target)
{
  target.gameKnob = source.gameKnob;
  target.started = source.started;
}

int getGameId(BoardState const &state)
{
  return (state.gameKnob * LED_COUNT) / 1024;
}

GameIntf *createGame(BoardState const &state)
{
  const int gameId = getGameId(state);
  switch (gameId) {
    case 0:
    case 1:
    return new snake::Snake();
    case 2:
    return new simon::Simon();
    case 3:
    return new guak::Guak();
    default:
    return new flood::Flood();
  }
}

void deleteGame(GameIntf *game)
{
  if (game != NULL)
  {
    delete game;
  }
}

// THE LOOP ()x()

GameIntf *game;
BoardState b1, b2;
BoardState *b;

void buildAndStartGame(BoardState const &state)
{
  board::lightAllOff();
  deleteGame(game);
  game = createGame(state);
  game->init();
}

void configBoardForLed(const int id)
{
  pinMode(ledPins[id], OUTPUT);
  pinMode(btnPins[id], INPUT_PULLUP);
}

void setup()
{
  randomSeed(analogRead(0));
  Serial.begin(9600);
  Serial.println("Coucou");

  board::forEachLed(&configBoardForLed);
  blinkAtStart(3);

  // Should actually be plugged to power
  powerUp(START_POWER);
  pinMode(START_PIN, INPUT);
  initBoard(b1);
  b = &b1;

  if (b1.started)
  {
    buildAndStartGame(b1);
  }
  else
  {
    const int initGameId = getGameId(*b);
    board::lightOn(initGameId);
  }
}

void switchGameState(
    BoardState const &prevBoard,
    BoardState &nextBoard)
{
  if (nextBoard.started != prevBoard.started)
  {
    if (nextBoard.started)
    {
      buildAndStartGame(nextBoard);
    }
    else
    {
      board::lightAllOff();
      board::lightOn(getGameId(nextBoard));
    }
  }
}

void playGameOrWait(
    BoardState const &prevBoard,
    BoardState &nextBoard)
{
  if (nextBoard.started)
  {
    const unsigned long currentMillis = millis();
    game->loop(currentMillis);
  }
  else
  {
    readSelectedGame(nextBoard);
    const int prevLed = getGameId(prevBoard);
    const int nextLed = getGameId(nextBoard);
    if (prevLed != nextLed)
    {
      board::lightOff(prevLed);
      board::lightOn(nextLed);
      Serial.print("Selected game = ");
      Serial.println(nextLed);
    }
  }
}

void loop()
{
  auto const start = millis();
  BoardState &prevBoard = *b;
  BoardState &nextBoard = b == &b1 ? b2 : b1;
  copyBoard(prevBoard, nextBoard);

  switchStart(nextBoard);
  switchGameState(prevBoard, nextBoard);
  playGameOrWait(prevBoard, nextBoard);

  b = b == &b1 ? &b2 : &b1;

  // We could do something smarter, keeping a 
  auto const end = millis();
  int const remaining = 10 - (end - start);
  if (remaining > 0) {
    delay(remaining);
  }
}
