#ifndef _BOARD_H_
#define _BOARD_H_

namespace board {

    void forEachLed(void (*f)(int));

    bool isPressed(const int id);

    void lightOn(const int id);
    void lightOff(const int id);
    
    void lightAllOn();
    void lightAllOff();
    
    bool hasLedPressed();

}

#endif
