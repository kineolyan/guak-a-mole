#include "./simon.h"

#include "Arduino.h"
#include "./board.h"

namespace simon
{
    void Simon::initState(GameState &state)
    {
        state.stage = INIT;
        Serial.println("=====");
        for (int i = 0; i < SEQ_SIZE; i += 1)
        {
            state.seq[i] = random(0, LED_COUNT);
            if (i < 5)
            {
                Serial.println(state.seq[i]);
            }
        }
        Serial.println("...\n=====");
        state.currentIdx = 0;
        state.endIdx = 1;
        state.nextTime = 0;
    }

    void copySeq(int const *src, int *dst)
    {
        for (int i = 0; i < SEQ_SIZE; i++)
        {
            *dst++ = *src++;
        }
    }

    void Simon::copy(GameState const &source, GameState &target)
    {
        if (target.stage == INIT)
        {
            copySeq(source.seq, target.seq);
        }
        target.stage = source.stage;
        target.currentIdx = source.currentIdx;
        target.endIdx = source.endIdx;
        target.nextTime = source.nextTime;
    }

    int getCurrentLight(GameState const &state)
    {
        return state.seq[state.currentIdx];
    }

    bool hasMoreLights(GameState const &state)
    {
        return state.currentIdx + 1 < state.endIdx;
    }

    void startGame(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        next.nextTime = currentMillis + 1000;
        next.stage = TEACH_OFF;
    }

    void teachNextLight(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }
        next.nextTime = currentMillis + 1500;
        next.stage = TEACH_ON;
        const int nextLight = getCurrentLight(previous);
        board::lightOn(nextLight);
    }

    void blinkBeforeNextStage(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }
        const auto litLed = getCurrentLight(previous);
        board::lightOff(litLed);
        if (hasMoreLights(previous))
        {
            next.currentIdx += 1;
            next.stage = TEACH_OFF;
            next.nextTime = currentMillis + 300;
        }
        else
        {
            next.stage = TEACH_COMPLETE;
            next.nextTime = currentMillis + 300;
        }
    }

    bool hasAnotherLightPressed(int const led)
    {
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            if (i != led && board::isPressed(i))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Tests that the LED is pressed, and only this LED.
     */
    bool isLightPressed(int const led)
    {
        return board::isPressed(led) && !hasAnotherLightPressed(led);
    }

    void playLossSequence()
    {
        for (int i = 0; i < 5; i += 1)
        {
            board::lightAllOn();
            delay(100);
            board::lightAllOff();
            delay(100);
        }
        board::lightAllOn();
    }

    void waitForPress(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }

        const auto lightToPress = getCurrentLight(previous);
        if (isLightPressed(lightToPress))
        {
            board::lightOn(lightToPress);
            next.stage = WAIT_RELEASE;
            next.nextTime = currentMillis + 100;
        }
        else if (hasAnotherLightPressed(lightToPress))
        {
            next.stage = DONE;
            playLossSequence();
        }
        // No time limit for this
    }

    bool hasSomeLightPressed()
    {
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            if (board::isPressed(i))
            {
                return true;
            }
        }
        return false;
    }

    bool isGameComplete(GameState const &state)
    {
        return state.endIdx == SEQ_SIZE;
    }

    void playWinRound()
    {
        for (int i = 0; i < 2; i += 1)
        {
            board::lightAllOn();
            delay(100);
            board::lightAllOff();
            delay(50);
        }
    }

    void playWinSequence()
    {
        board::lightAllOff();
        for (int i = 50, led = 0; i > 0; i -= 1)
        {
            board::lightOff(led);
            led = (led + 1) % LED_COUNT;
            board::lightOn(led);
            delay(10 * i);
        }
    }

    void waitForRelease(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }

        if (hasSomeLightPressed())
        {
            return;
        }

        board::lightAllOff(); // Could off only the current light
        if (hasMoreLights(previous))
        {
            next.currentIdx += 1;
            next.stage = WAIT_PRESS;
            next.nextTime = currentMillis + 50;
        }
        else
        {
            next.nextTime = currentMillis + 200;
            next.stage = WON_ROUND;
        }
    }

    void signalTeachComplete(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }
        for (int i = 0; i < 1; i += 1)
        {
            board::lightAllOn();
            delay(100);
            board::lightAllOff();
            delay(50);
        }

        next.stage = WAIT_PRESS;
        next.currentIdx = 0;
        // No need to update time as we want to catch a press as soon as possible
    }

    void congratForRound(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        if (previous.nextTime > currentMillis)
        {
            return;
        }
        if (isGameComplete(previous))
        {
            playWinSequence();
        }
        else
        {
            next.currentIdx = 0;
            next.endIdx += 1;
            next.nextTime = currentMillis + 750;
            next.stage = TEACH_OFF;
            playWinRound();
        }
    }

    void Simon::doLoop(
        const unsigned long currentMillis,
        GameState const &previous,
        GameState &next)
    {
        switch (next.stage)
        {
        case INIT:
            return startGame(currentMillis, previous, next);
        case TEACH_OFF:
            return teachNextLight(currentMillis, previous, next);
        case TEACH_ON:
            return blinkBeforeNextStage(currentMillis, previous, next);
        case TEACH_COMPLETE:
            return signalTeachComplete(currentMillis, previous, next);
        case WAIT_PRESS:
            return waitForPress(currentMillis, previous, next);
        case WAIT_RELEASE:
            return waitForRelease(currentMillis, previous, next);
        case WON_ROUND:
            return congratForRound(currentMillis, previous, next);
        case DONE:
            return; // Nothing to do
        }
    }

} // namespace simon