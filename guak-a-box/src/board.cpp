#include "Arduino.h"

#include "board.h"
#include "./constants.h"

namespace board
{

    void forEachLed(void (*f)(int))
    {
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            (*f)(i);
        }
    }

    /**
     * @brief Tests if the LED is pressed.
     */
    bool isPressed(const int id)
    {
        const int sensorVal = digitalRead(btnPins[id]);
        return sensorVal != HIGH;
    }

    void lightOn(const int id)
    {
        digitalWrite(ledPins[id], HIGH);
    }
    void lightOff(const int id)
    {
        digitalWrite(ledPins[id], LOW);
    }

    void lightAllOn()
    {
        forEachLed(&lightOn);
    }
    void lightAllOff()
    {
        forEachLed(&lightOff);
    }

    /**
     * @brief Tests if any LED is pressed.
     */
    bool hasLedPressed()
    {
        for (int i = 0; i < LED_COUNT; i += 1)
        {
            if (isPressed(i))
            {
                return true;
            }
        }
        return false;
    }

} // namespace board