#define LED_COUNT 5

const int ledPins[] =  {
  13, // green
  11, // red
  12, // white
  9,  // blue
  10  // yellow
};
const int btnPins[] = {
  6, // green
  4, // red
  5, // white
  2, // blue
  3  // yellow
};

enum State {
  LIT, // Lit in an active game
  WAITING, // Waiting to be lit
  PRESSED, // Button is pressed
  BLINKING // Playing a seq for loss
};

typedef struct LedStates {
  State state;
  unsigned long targetTime;
  char lossSeq; // States for loss blinking sequences
} LedState;

bool isPressed(const int id) {
  const int sensorVal = digitalRead(btnPins[id]);
  return sensorVal != HIGH;
}

void lightOn(const int id) {
  digitalWrite(ledPins[id], HIGH);
}
void lightOff(const int id) {
  digitalWrite(ledPins[id], LOW);
}

void forEachLed(void (*f)(int)) {
  for (int i = 0; i < LED_COUNT; i += 1) {
    (*f)(i);
  }
}

void lightAllOn() {
  forEachLed(&lightOn);
}
void lightAllOff() {
  forEachLed(&lightOff);
}

void setupNextLit(
  const unsigned long currentTime,
  LedState& led
) {
  const long nextTime = random(10000, 15000);
  led.state = WAITING;
  led.targetTime = currentTime + nextTime;
}

void setupBlinking(
  const unsigned long currentTime,
  LedState& led
) {
  led.lossSeq = 5;
  led.state = BLINKING;
}

/**
 * Plays a sequence of blinking to indicate that the LED expired.
 * @return true at the end of the sequence
 */
void playLossBlink(
  const int id,
  const LedState& led
) {
  const char state = led.lossSeq;
  if (state == 0) {
    return;
  }

  if (state > 0) {
    lightOn(id);
  } else if (state < 0) {
    lightOff(id);
  }
}

void udpateLossSeq(LedState& led) {
  const char state = led.lossSeq;
  if (state > 0) {
    led.lossSeq = ~state;
  } else {
    led.lossSeq = ~state - 1;
  }
}

void awaitPressOrTimeout(
  const unsigned long currentTime, 
  const int id,
  LedState& led) 
{
  // Favor win over loss, test first for press instead of timeout
  // It is possible that both occurred since the last loop
  if (isPressed(id)) {
    lightOff(id);
    led.state = PRESSED;
  } else if (led.targetTime >= currentTime) {
    setupBlinking(currentTime, led);
    playLossBlink(id, led);
    udpateLossSeq(led);
  }
}

void waitTimeToLit(
  const unsigned long currentTime, 
  const int id,
  LedState& led
) {
  if (led.targetTime >= currentTime) {
    lightOn(id);
    led.state = LIT;
    const long litDuration = random(3000, 5000);
    led.targetTime = currentTime + litDuration;
  }
}

void awaitRelease(
  const unsigned long currentTime, 
  const int id,
  LedState& led
) {
  if (!isPressed(id)) {
    setupNextLit(currentTime, led);
  }
}

void playBlinkingSequence(
    const unsigned long currentTime, 
    const int id,
    LedState& led
) {
  if (led.targetTime >= currentTime) {
    playLossBlink(id, led);
    udpateLossSeq(led);
    if (led.lossSeq == 0) {
      setupNextLit(currentTime, led);
    } else {
      led.targetTime = currentTime + 100;
    }
  }  
}

void loopOnBlock(
  const unsigned long currentTime, 
  const int id,
  LedState& led
) {
  switch (led.state) {
    case LIT: 
      awaitPressOrTimeout(currentTime, id, led);
      break;
    case WAITING: 
      waitTimeToLit(currentTime, id, led);
      break;
    case PRESSED: 
      awaitRelease(currentTime, id, led);
      break;
    case BLINKING:  
      playBlinkingSequence(currentTime, id, led);
      break;
  }
}

void blinkAtStart(const int n) {
  for (int i = 0; i < n; i += 1) {
    if (i > 0) {
      delay(50);
    }
    lightAllOn();
    delay(100);
    lightAllOff();
  }
}

// THE LOOP (-)v(-)

LedState leds[LED_COUNT];

void configBoardForLed(const int id) {
  pinMode(ledPins[id], OUTPUT);
  pinMode(btnPins[id], INPUT_PULLUP);
}
void initLedState(const int id) {
  LedState& led = leds[id];
  setupNextLit(millis(), led);
}

void setup() {
  // Serial.begin(9600);
  // Serial.println("Start guak");

  forEachLed(&configBoardForLed);
  forEachLed(&initLedState);

  blinkAtStart(5);
}

void loop() {
  const unsigned long currentMillis = millis();
  for (int i = 0; i < LED_COUNT; i += 1) {
    loopOnBlock(currentMillis, i, leds[i]);
  }
}
