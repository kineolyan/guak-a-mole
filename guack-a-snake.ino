#define LED_COUNT 5

const int ledPins[] =  {
  13, // green
  11, // red
  12, // white
  9,  // blue
  10  // yellow
};
const int btnPins[] = {
  6, // green
  4, // red
  5, // white
  2, // blue
  3  // yellow
};

typedef struct State {
    LIT,
    PRESSED,
    WAIT_CLEAR
}

typedef struct GameState {
    State state;
    int previousLed;
    int targetLed;
} GameState;

bool isPressed(const int id) {
  const int sensorVal = digitalRead(btnPins[id]);
  return sensorVal != HIGH;
}

void lightOn(const int id) {
  digitalWrite(ledPins[id], HIGH);
}
void lightOff(const int id) {
  digitalWrite(ledPins[id], LOW);
}

void forEachLed(void (*f)(int)) {
  for (int i = 0; i < LED_COUNT; i += 1) {
    (*f)(i);
  }
}

void lightAllOn() {
  forEachLed(&lightOn);
}
void lightAllOff() {
  forEachLed(&lightOff);
}

boolean hasLedPressed() {
    for (int i = 0; i < LED_COUNT; i += 1) {
        if (isPressed(i)) {
            return true;
        }
    }
    return false;
}

void pickNextLed(const int currentLed) {
    while (true) {
        const int targetLed = random(0, LED_COUNT);
        if (targetLed != currentLed) {
            return targetLed;
        }
    }
}

void setupNextLit(GameState& game) {
    game.previousLed = game.targetLed;
    game.targetLed = pickNextLed(game.previousLed);
    game.state = PRESSED;
}

void awaitPress(GameState& game) {
  if (isPressed(game.targetLed)) {
    setupNextLit(game);
    lightOn(game.targetLed);
  }
}

void awaitRelease(GameState& game) {
  if (!isPressed(game.previousLed)) {
    lightOff(game.previousLed);
    if (!hasLedPressed()) {
        game.state = LIT;
    } else {
        game.state = WAIT_CLEAR;
    }
  }
}

void awaitClear(GameState& game) {
  if (!hasLedPressed()) {
    game.state = LIT;
  }
}

void doLoop(
  const unsigned long currentTime, 
  GameState& game
) {
  switch (game.state) {
    case LIT: 
      awaitPress(game);
      break;
    case PRESSED: 
      awaitRelease(game);
      break;
    case WAIT_CLEAR: 
      awaitClear(game);
      break;
  }
}

void blinkAtStart(const int n) {
  for (int i = 0; i < n; i += 1) {
    if (i > 0) {
      delay(50);
    }
    lightAllOn();
    delay(100);
    lightAllOff();
  }
}

void initState(GameState& gameState) {
    gameState.state = LIT;
    gameState.targetLed = pickNextLed(-1);
    lightOn(gameState.targetLed);
}

// THE LOOP (-)v(-)

GameState state;

void configBoardForLed(const int id) {
  pinMode(ledPins[id], OUTPUT);
  pinMode(btnPins[id], INPUT_PULLUP);
}

void setup() {
//   Serial.begin(9600);

  forEachLed(&configBoardForLed);
  blinkAtStart(5);

  initState(state);
}

void loop() {
  const unsigned long currentMillis = millis();
  doLoop(currentMillis, state);
}
